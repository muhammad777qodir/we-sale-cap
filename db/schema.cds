namespace my.bookshop;

using {managed, cuid} from '@sap/cds/common';

@assert.unique : {
name : [name]
}
entity FullBaseModel : managed, cuid{
   name : String;
   deleted : Boolean default false;
}

@assert.unique : {
name : [name]
}
entity BaseModel : cuid{
   name : String;
   deleted : Boolean default false;
}

entity Book : FullBaseModel{
   writtenLanguage : String;
   summary : String;
   count : Integer;
   sellingPrice : Decimal not null;
   author : Association to Author;
   bookType : Association to BookType;
   photo : Association to Attachment;
   file : Association to Attachment;
   comments : Association to many Comment on comments.book = $self;
}

entity Author : FullBaseModel{
   likes : Integer;
   books : Association to many Book on books.author = $self;
}

entity Users : managed, cuid{
   phoneNumber : String;
   password : String;
   books : Association to many UserBook on books.owner = $self;
   avatar : Association to Attachment;
}

entity UserBook : cuid{
   book : Association to Book;
   owner : Association to Users;
}

entity Attachment : BaseModel{
   content : LargeBinary;
   mediaType : String;
   filename : String;
}

entity BookType : BaseModel{}

entity Comment : FullBaseModel{
   book : Association to Book;
}

entity Student : cuid, managed{
   name : String;
   password : String ;
   email : String;
   count : Integer;
   school : Association to School;
}

entity School : cuid , managed{
   name : String;
   password : String;
   email : String;
   students : Association to many Student on students.school = $self;
}
