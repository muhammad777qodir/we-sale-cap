package cap.sap.com.wesale.handlers;

import cap.sap.com.wesale.reposiroty.AuthorRepository;
import cds.gen.my.bookshop.Author;
import cds.gen.catalogservice.CatalogService_;
import cds.gen.catalogservice.DeleteAuthorContext;
import com.sap.cds.services.ServiceException;
import com.sap.cds.services.cds.CdsCreateEventContext;
import com.sap.cds.services.cds.CdsService;
import com.sap.cds.services.handler.EventHandler;
import com.sap.cds.services.handler.annotations.On;
import com.sap.cds.services.handler.annotations.ServiceName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@Component
@RequiredArgsConstructor
@ServiceName(CatalogService_.CDS_NAME)
public class AuthorServiceHandler implements EventHandler {

    private final AuthorRepository authorRepository;

    @On(event = CdsService.EVENT_CREATE, entity = cds.gen.catalogservice.Author_.CDS_NAME)
    public void add(CdsCreateEventContext context) {

        Author author = Author.create();

        for (Map<String, Object> reqBody : context.getCqn().entries()) {
            if (!reqBody.isEmpty()) {

                if (reqBody.containsKey(Author.ID)) {
                    Optional<Author> optional = authorRepository
                            .findByName(reqBody.get(Author.ID).toString());
                    if (optional.isPresent()) {
                        author = optional.get();
                    } else {
                        context
                                .getMessages()
                                .info("Author did not found with ID:", reqBody.get(Author.ID))
                                .code("207");
                        context.setCompleted();
                        throw new RuntimeException("Author did not found with ID:" + reqBody.get(Author.ID));
                    }
                }

                if (reqBody.containsKey(Author.NAME)) {
                    Optional<Author> optionalAuthor = authorRepository
                            .findByName(reqBody.get(Author.NAME).toString());
                    if (optionalAuthor.isPresent()) {
                        context
                                .getMessages()
                                .info("Author already exist with name:", reqBody.get(Author.NAME))
                                .code("209");
                        context.setCompleted();
                        throw new RuntimeException("Author already exist with name:" + reqBody.get(Author.NAME));
                    }
                    author.setName(reqBody.get(Author.NAME).toString());
                }
            }

            if (reqBody.containsKey(Author.ID)) {
                authorRepository.update(author);
            } else {
                authorRepository.save(author);
            }

        }
        context.setResult(Collections.emptyList());
        context.setCompleted();
    }

    @On(event = DeleteAuthorContext.CDS_NAME)
    public void deleteAuthor(DeleteAuthorContext context){
        Optional<Author> author = authorRepository.findById(context.getId());
        if (author.isPresent()){
            author.get().setDeleted(true);
            authorRepository.update(author.get());
        }else {
            context
                    .getMessages()
                    .info("Author did not found",context.getId())
                    .code("207");
            context.setCompleted();
            throw new ServiceException("Author did not found with id:".concat(context.getId()));
        }
        context.setCompleted();
    }

}
