package cap.sap.com.wesale.reposiroty.impl;

import cap.sap.com.wesale.reposiroty.AttachmentRepository;
import cds.gen.my.bookshop.Attachment;
import cds.gen.my.bookshop.Attachment_;
import com.sap.cds.Result;
import com.sap.cds.ql.Insert;
import com.sap.cds.ql.Select;
import com.sap.cds.ql.Update;
import com.sap.cds.services.persistence.PersistenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AttachmentRepositoryImpl implements AttachmentRepository {

    private final PersistenceService db;

    @Override
    public Result save(Attachment obj) {
        Insert query = Insert
                .into(Attachment_.class)
                .entry(obj);

        return db.run(query);
    }

    @Override
    public Result update(Attachment obj) {
        Update<Attachment_> query = Update
                .entity(Attachment_.class)
                .data(obj);

        return db.run(query);
    }

    @Override
    public Optional<Attachment> findByName(String name) {
        Select<Attachment_> query = Select
                .from(Attachment_.class)
                .where(attachment -> attachment.name().eq(name));

        return db.run(query).first(Attachment.class);
    }

    @Override
    public Optional<Attachment> findById(String id) {
        Select<Attachment_> query = Select
                .from(Attachment_.class)
                .where(attachment -> attachment.ID().eq(id));

        return db.run(query).first(Attachment.class);
    }

    @Override
    public boolean existByName(String name) {
        Select<Attachment_> query = Select
                .from(Attachment_.class)
                .where(attachment -> attachment.name().eq(name));

        Optional<Attachment> optional = db.run(query).first(Attachment.class);
        if (optional.isPresent()){
            return true;
        }else {
            return false;
        }
    }
}
