package cap.sap.com.wesale.reposiroty;

import com.sap.cds.Result;

import java.util.Optional;

public interface DbBase<T> {

    Result save(T obj);

    Result update(T obj);

    Optional<T> findByName(String name);

    Optional<T> findById(String id);

    boolean existByName(String name);

}
