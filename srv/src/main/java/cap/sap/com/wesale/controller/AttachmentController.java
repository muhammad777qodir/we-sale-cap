package cap.sap.com.wesale.controller;

import cap.sap.com.wesale.constants.EndpointConstants;
import cap.sap.com.wesale.service.AttachmentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;

@RestController
@RequiredArgsConstructor
@RequestMapping(EndpointConstants.attachmentSource)
public class AttachmentController {

    private final AttachmentService attachmentService;

    @GetMapping(EndpointConstants.byId)
    public ResponseEntity<?> getById(@PathVariable String id){
        return ResponseEntity.ok(attachmentService.getById(id));
    }

    @PostMapping
    public ResponseEntity<?> add(MultipartHttpServletRequest request) throws IOException {
        return ResponseEntity.ok(attachmentService.add(request));
    }

}
