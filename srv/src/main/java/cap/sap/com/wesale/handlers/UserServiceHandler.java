package cap.sap.com.wesale.handlers;

import cap.sap.com.wesale.reposiroty.UserRepository;
import cds.gen.catalogservice.CatalogService_;
import com.sap.cds.services.handler.EventHandler;
import com.sap.cds.services.handler.annotations.On;
import com.sap.cds.services.handler.annotations.ServiceName;
import com.sap.cds.services.request.UserInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@RequiredArgsConstructor
@ServiceName(CatalogService_.CDS_NAME)
public class UserServiceHandler implements EventHandler{

    private final UserRepository userRepository;

    @On(event = cds.gen.catalogservice.GetUserInfoContext.CDS_NAME)
    public void getUserInfo(cds.gen.catalogservice.GetUserInfoContext context){

        cds.gen.catalogservice.UserInformation user = cds.gen.catalogservice.UserInformation.create();
        UserInfo userInfo = context.getUserInfo();

        String name = userInfo.getName();
        String id = userInfo.getId();
        Set<String> roles = userInfo.getRoles();
        String tenant = userInfo.getTenant();

        user.setName(name);
        user.setId(id);
        user.setRoles(roles.toString());
        user.setTenant(tenant);

        context.setResult(user);
        context.setCompleted();
    }

}
