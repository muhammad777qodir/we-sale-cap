package cap.sap.com.wesale.handlers;

import cap.sap.com.wesale.enums.LanguagesEnum;
import cap.sap.com.wesale.reposiroty.AuthorRepository;
import cds.gen.catalogservice.DeleteBookContext;
import cds.gen.my.bookshop.Book;
import cds.gen.my.bookshop.BookType;
import cap.sap.com.wesale.reposiroty.AttachmentRepository;
import cap.sap.com.wesale.reposiroty.BookRepository;
import cap.sap.com.wesale.reposiroty.BookTypeRepository;
import cds.gen.catalogservice.CatalogService_;
import com.sap.cds.services.cds.CdsCreateEventContext;
import com.sap.cds.services.cds.CdsService;
import com.sap.cds.services.handler.EventHandler;
import com.sap.cds.services.handler.annotations.On;
import com.sap.cds.services.handler.annotations.ServiceName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@Component
@ServiceName(CatalogService_.CDS_NAME)
@RequiredArgsConstructor
public class BookServiceHandler implements EventHandler{

    private final BookRepository bookRepository;

    private final AuthorRepository authorRepository;

    private final BookTypeRepository bookTypeRepository;

    private final AttachmentRepository attachmentRepository;

    @On(event = CdsService.EVENT_CREATE, entity = cds.gen.catalogservice.Book_.CDS_NAME)
    public void add(CdsCreateEventContext context) {

        Book book = Book.create();

        for (Map<String, Object> reqBody : context.getCqn().entries()) {
            if (!reqBody.isEmpty()) {

                if (reqBody.containsKey(Book.ID)) {
                    Optional<Book> optionalBook = bookRepository
                            .findById(reqBody.get(Book.ID).toString());
                    if (optionalBook.isPresent()) {
                        book = optionalBook.get();
                    } else {
                        context.getMessages().info("Name already exist ").code("209");
                        context.setCompleted();
                        throw new RuntimeException("Name already exist ");
                    }
                }

                if (reqBody.containsKey(Book.NAME)) {
                    boolean existByName = bookRepository
                            .existByName(reqBody.get(Book.NAME).toString());
                    if (existByName) {
                        context.getMessages().info("Name already exist ").code("209");
                        context.setCompleted();
                        throw new RuntimeException("Name already exist ");
                    }
                    book.setName(reqBody.get(Book.NAME).toString());
                } else if (!reqBody.containsKey(Book.ID)) {
                    context.getMessages().warn("Name mandatory ").code("207");
                    context.setCompleted();
                    throw new RuntimeException("Name mandatory ");
                }

                if (reqBody.containsKey(Book.WRITTEN_LANGUAGE)) {
                    book.setWrittenLanguage(
                            LanguagesEnum.valueOf(reqBody.get(Book.WRITTEN_LANGUAGE).toString()).name()
                    );
                }

                if (reqBody.containsKey(Book.SUMMARY)) {
                    book.setSummary(reqBody.get(Book.SUMMARY).toString());
                }

                if (reqBody.containsKey(Book.COUNT)) {
                    book.setCount((Integer) reqBody.get(Book.COUNT));
                }

                if (reqBody.containsKey(Book.SELLING_PRICE)) {
                    book.setSellingPrice((BigDecimal) reqBody.get(Book.SELLING_PRICE));
                }

                if (reqBody.containsKey(Book.AUTHOR_ID)) {
                    Optional<cds.gen.my.bookshop.Author> isPresent = authorRepository
                            .findById(reqBody.get(Book.AUTHOR_ID).toString());
                    if (!isPresent.isPresent()) {
                        context.getMessages().info("Author did not found with id:", reqBody.get(Book.AUTHOR_ID)).code("209");
                        context.setCompleted();
                        throw new RuntimeException("Author did not found with id:" + reqBody.get(Book.AUTHOR_ID));
                    }
                    book.setAuthorId(isPresent.get().getId());
                }

                if (reqBody.containsKey(Book.BOOK_TYPE_ID)) {
                    Optional<BookType> bookTypes = bookTypeRepository
                            .findById(reqBody.get(Book.BOOK_TYPE_ID).toString());
                    if (!bookTypes.isPresent()) {
                        context.getMessages().info("BookType did not found with id : ", reqBody.get(Book.BOOK_TYPE_ID));
                        context.setCompleted();
                        throw new RuntimeException("BookType did not found with id : " + reqBody.get(Book.BOOK_TYPE_ID));
                    }
                    book.setBookTypeId(bookTypes.get().getId());
                }

                if (reqBody.containsKey(Book.PHOTO_ID)) {
                    Optional<cds.gen.my.bookshop.Attachment> attachment = attachmentRepository
                            .findById(reqBody.get(Book.PHOTO_ID).toString());
                    if (!attachment.isPresent()) {
                        context.getMessages().info("Attachment did not found with id : ", reqBody.get(Book.PHOTO_ID));
                        context.setCompleted();
                        throw new RuntimeException("Attachment did not found with id : " + reqBody.get(Book.PHOTO_ID));
                    }
                    book.setPhotoId(attachment.get().getId());
                }

                if (reqBody.containsKey(Book.FILE_ID)) {
                    Optional<cds.gen.my.bookshop.Attachment> attachment = attachmentRepository
                            .findById(reqBody.get(Book.FILE_ID).toString());
                    if (!attachment.isPresent()) {
                        context.getMessages().info("Attachment did not found with id : ", reqBody.get(Book.FILE_ID));
                        context.setCompleted();
                        throw new RuntimeException("Attachment did not found with id : " + reqBody.get(Book.FILE_ID));
                    }
                    book.setPhotoId(attachment.get().getId());
                }

                if (reqBody.containsKey(Book.ID)) {
                    bookRepository.update(book);
                } else {
                    bookRepository.save(book);
                }

            }
        }
        context.setResult(Collections.emptyList());
        context.setCompleted();
    }

    @On(event = DeleteBookContext.CDS_NAME)
    public void deleteBook(DeleteBookContext context){

        Optional<Book> book = bookRepository.findById(context.getId());
        if (book.isPresent()){
            book.get().setDeleted(true);
            bookRepository.update(book.get());
        }else {
            context.getMessages().info("Book did not found with id : ", context.getId());
            context.setCompleted();
            throw new RuntimeException("Book did not found with id : " + context.getId());
        }
        context.setResult("Deleted");
        context.setCompleted();
    }

}
