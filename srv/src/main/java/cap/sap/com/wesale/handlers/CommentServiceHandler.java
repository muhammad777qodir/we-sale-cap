package cap.sap.com.wesale.handlers;

import cap.sap.com.wesale.reposiroty.CommentRepository;
import cds.gen.catalogservice.CatalogService_;
import cds.gen.catalogservice.DeleteCommentContext;
import com.sap.cds.services.handler.EventHandler;
import com.sap.cds.services.handler.annotations.On;
import com.sap.cds.services.handler.annotations.ServiceName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
@ServiceName(CatalogService_.CDS_NAME)
public class CommentServiceHandler implements EventHandler{

    private final CommentRepository commentRepository;

    @On(event = DeleteCommentContext.CDS_NAME)
    public void deleteComment(DeleteCommentContext context){

        Optional<cds.gen.my.bookshop.Comment> comment = commentRepository.findById(context.getId());
        if (comment.isPresent()){
            comment.get().setDeleted(true);
            commentRepository.update(comment.get());
        }else {
            context.getMessages().info("error").code("207");
            context.setCompleted();
        }
        context.setCompleted();
    }

}
