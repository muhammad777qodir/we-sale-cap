package cap.sap.com.wesale.reposiroty.impl;

import cap.sap.com.wesale.reposiroty.CommentRepository;
import com.sap.cds.Result;
import com.sap.cds.ql.Insert;
import com.sap.cds.ql.Select;
import com.sap.cds.ql.Update;
import com.sap.cds.services.persistence.PersistenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CommentRepositoryImpl implements CommentRepository {

    private final PersistenceService db;

    @Override
    public Result save(cds.gen.my.bookshop.Comment obj) {
        Insert query = Insert
                .into(cds.gen.my.bookshop.Comment_.class)
                .entry(obj);

        return db.run(query);
    }

    @Override
    public Result update(cds.gen.my.bookshop.Comment obj) {
        Update<cds.gen.my.bookshop.Comment_> query = Update
                .entity(cds.gen.my.bookshop.Comment_.class)
                .data(obj);

        return db.run(query);
    }

    @Override
    public Optional<cds.gen.my.bookshop.Comment> findByName(String name) {
        return Optional.empty();
    }

    @Override
    public Optional<cds.gen.my.bookshop.Comment> findById(String id) {
        Select<cds.gen.my.bookshop.Comment_> query = Select
                .from(cds.gen.my.bookshop.Comment_.class)
                .where(comment -> comment.ID().eq(id));

        return db.run(query).first(cds.gen.my.bookshop.Comment.class);
    }

    @Override
    public boolean existByName(String name) {
        return false;
    }

}
