package cap.sap.com.wesale.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {

    private String message;

    private Long code;

    private Object object;

    public ApiResponse(String message, Long code) {
        this.message = message;
        this.code = code;
    }
}

