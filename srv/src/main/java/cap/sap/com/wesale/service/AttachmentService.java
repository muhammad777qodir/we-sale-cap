package cap.sap.com.wesale.service;

import cap.sap.com.wesale.dto.ApiResponse;
import cds.gen.my.bookshop.Attachment;
import cap.sap.com.wesale.reposiroty.AttachmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.util.Iterator;

@Service
@RequiredArgsConstructor
public class AttachmentService {

    private final AttachmentRepository attachmentRepository;

    public ApiResponse add(MultipartHttpServletRequest request) throws IOException {

        Attachment attachment = Attachment.create();
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file;

        while (fileNames.hasNext()){
            file = request.getFile(fileNames.next());
            attachment.setName(file.getName());
            attachment.setContent(file.getBytes());
            attachmentRepository.save(attachment);
        }

        return new ApiResponse("Success",200L);
    }

    public HttpEntity<?> getById(String id){
        Attachment attachment = attachmentRepository
                .findById(id)
                .orElseThrow(() -> new RuntimeException("File not found"));

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachment.getMediaType()))
                .body(attachment.getContent());
    }

}
