package cap.sap.com.wesale.reposiroty.impl;

import cap.sap.com.wesale.reposiroty.AuthorRepository;
import cds.gen.my.bookshop.Author;
import com.sap.cds.Result;
import com.sap.cds.ql.Insert;
import com.sap.cds.ql.Select;
import com.sap.cds.ql.Update;
import com.sap.cds.services.persistence.PersistenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class AuthorRepositoryImpl implements AuthorRepository {

    private final PersistenceService db;

    @Override
    public Result save(Author obj) {
        Insert query = Insert
                .into(cds.gen.my.bookshop.Author_.class)
                .entry(obj);

        return db.run(query);
    }

    @Override
    public Result update(Author obj) {
        Update<cds.gen.my.bookshop.Author_> query = Update
                .entity(cds.gen.my.bookshop.Author_.class)
                .data(obj);

        return db.run(query);
    }

    @Override
    public Optional<Author> findByName(String name) {
        Select<cds.gen.my.bookshop.Author_> query = Select
                .from(cds.gen.my.bookshop.Author_.class)
                .where(author -> author.name().eq(name));

        return db.run(query).first(Author.class);
    }

    @Override
    public Optional<Author> findById(String id) {
        Select<cds.gen.my.bookshop.Author_> query = Select
                .from(cds.gen.my.bookshop.Author_.class)
                .where(author -> author.ID().eq(id));

        return db.run(query).first(Author.class);
    }

    @Override
    public boolean existByName(String name) {
        return false;
    }
}
