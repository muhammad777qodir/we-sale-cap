package cap.sap.com.wesale.reposiroty.impl;

import cap.sap.com.wesale.reposiroty.BookTypeRepository;
import cds.gen.my.bookshop.BookType;
import com.sap.cds.Result;
import com.sap.cds.ql.Insert;
import com.sap.cds.ql.Select;
import com.sap.cds.ql.Update;
import com.sap.cds.services.persistence.PersistenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class BookTypeRepositoryImpl implements BookTypeRepository {

    private final PersistenceService db;

    @Override
    public Result save(BookType obj) {
        Insert insert = Insert
                .into(cds.gen.my.bookshop.BookType_.class)
                .entry(obj);

        return db.run(insert);
    }

    @Override
    public Result update(BookType obj) {
        Update<cds.gen.my.bookshop.BookType_> query = Update
                .entity(cds.gen.my.bookshop.BookType_.class)
                .data(obj);

        return db.run(query);
    }

    @Override
    public Optional<BookType> findByName(String name) {
        return Optional.empty();
    }

    @Override
    public Optional<BookType> findById(String id) {
        Select<cds.gen.my.bookshop.BookType_> select = Select
                .from(cds.gen.my.bookshop.BookType_.class)
                .where(bookType -> bookType.ID().eq(id));

        return db.run(select).first(BookType.class);
    }

    @Override
    public boolean existByName(String name) {
        Select<cds.gen.my.bookshop.BookType_> query = Select
                .from(cds.gen.my.bookshop.BookType_.class)
                .where(bookType -> bookType.name().eq(name));

        Optional<BookType> optionalBookType = db.run(query).first(BookType.class);

        if (optionalBookType.isPresent()){
            return true;
        }
        return false;
    }
}
