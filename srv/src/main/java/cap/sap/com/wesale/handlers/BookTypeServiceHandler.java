package cap.sap.com.wesale.handlers;

import cap.sap.com.wesale.reposiroty.BookTypeRepository;
import cds.gen.my.bookshop.BookType;
import cds.gen.catalogservice.BookType_;
import cds.gen.catalogservice.CatalogService_;
import cds.gen.catalogservice.DeleteBookTypeContext;
import com.sap.cds.services.ServiceException;
import com.sap.cds.services.cds.CdsCreateEventContext;
import com.sap.cds.services.cds.CdsService;
import com.sap.cds.services.handler.EventHandler;
import com.sap.cds.services.handler.annotations.On;
import com.sap.cds.services.handler.annotations.ServiceName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@RequiredArgsConstructor
@ServiceName(CatalogService_.CDS_NAME)
public class BookTypeServiceHandler implements EventHandler {

    private final BookTypeRepository bookTypeRepository;

    @On(event = CdsService.EVENT_CREATE, entity = BookType_.CDS_NAME)
    public void add(CdsCreateEventContext context){

        BookType bookType = BookType.create();

        for (Map<String, Object> reqBody : context.getCqn().entries()) {

            if (reqBody.containsKey(BookType.ID)){
                Optional<BookType> optionalBookType = bookTypeRepository
                        .findById(reqBody.get(BookType.ID).toString());
                if (optionalBookType.isPresent()){
                    bookType = optionalBookType.get();
                }else {
                    context
                            .getMessages()
                            .info("BookType did not found with id:",reqBody.get(BookType.ID))
                            .code("209");
                    context.setCompleted();
                    throw new RuntimeException("BookType did not found with id:".concat(reqBody.get(BookType.ID).toString()));
                }
            }

            if (reqBody.containsKey(BookType.NAME)){
                boolean existByName = bookTypeRepository
                        .existByName(reqBody.get(BookType.NAME).toString());
                if (existByName){
                    context
                            .getMessages()
                            .info("BookType already exist with name:",reqBody.get(BookType.NAME))
                            .code("207");
                    context.setCompleted();
                    throw new RuntimeException(
                            "BookType did not found with id:".concat(reqBody.get(BookType.NAME).toString())
                    );
                }
                bookType.setName(reqBody.get(BookType.NAME).toString());
            }

        }

        bookTypeRepository.save(bookType);
        context.setResult(Collections.emptyList());
        context.setCompleted();
    }

    @On(event = DeleteBookTypeContext.CDS_NAME)
    public void deleteBookType(DeleteBookTypeContext context){

        Optional<BookType> optionalBookType = bookTypeRepository.findById(context.getId());
        if (optionalBookType.isPresent()){
            optionalBookType.get().setDeleted(true);
            bookTypeRepository.update(optionalBookType.get());
        }else {
            context.getMessages().info("error",context.getId());
            context.setCompleted();
            throw new ServiceException("error");
        }
        context.setCompleted();
    }

}
