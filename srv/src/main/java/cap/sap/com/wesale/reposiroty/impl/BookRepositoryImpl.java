package cap.sap.com.wesale.reposiroty.impl;

import cap.sap.com.wesale.reposiroty.BookRepository;
import cds.gen.my.bookshop.Book;
import com.sap.cds.Result;
import com.sap.cds.ql.Insert;
import com.sap.cds.ql.Select;
import com.sap.cds.ql.Update;
import com.sap.cds.services.persistence.PersistenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class BookRepositoryImpl implements BookRepository {

    private final PersistenceService db;

    @Override
    public Result save(Book obj) {
        Insert query = Insert
                .into(cds.gen.my.bookshop.Book_.class)
                .entry(obj);

        return db.run(query);
    }

    @Override
    public Result update(Book obj) {
        Update<cds.gen.my.bookshop.Book_> query = Update
                .entity(cds.gen.my.bookshop.Book_.class)
                .data(obj);

        return db.run(query);
    }

    @Override
    public Optional<Book> findByName(String name) {
        return Optional.empty();
    }

    @Override
    public Optional<Book> findById(String id) {
        Select<cds.gen.my.bookshop.Book_> query = Select
                .from(cds.gen.my.bookshop.Book_.class)
                .where(book -> book.ID().eq(id));

        return db.run(query).first(Book.class);
    }

    @Override
    public boolean existByName(String name) {
        Select<cds.gen.my.bookshop.Book_> query = Select
                .from(cds.gen.my.bookshop.Book_.class)
                .where(book -> book.name().eq(name));

        Optional<Book> optionalBook = db.run(query).first(Book.class);

        if (optionalBook.isPresent()){
            return true;
        }
        return false;
    }
}
