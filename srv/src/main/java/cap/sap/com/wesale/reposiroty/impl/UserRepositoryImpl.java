package cap.sap.com.wesale.reposiroty.impl;

import cap.sap.com.wesale.reposiroty.UserRepository;
import cds.gen.my.bookshop.Users;
import cds.gen.my.bookshop.Users_;
import com.sap.cds.Result;
import com.sap.cds.ql.Insert;
import com.sap.cds.ql.Select;
import com.sap.cds.ql.Update;
import com.sap.cds.services.persistence.PersistenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private final PersistenceService db;

    @Override
    public Result save(Users obj) {
        Insert query = Insert
                .into(Users_.class)
                .entry(obj);

        return db.run(query);
    }

    @Override
    public Result update(Users obj) {
        Update<Users_> query = Update
                .entity(Users_.class)
                .data(obj);

        return db.run(query);
    }

    @Override
    public Optional<Users> findByName(String name) {
        return Optional.empty();
    }

    @Override
    public Optional<Users> findById(String id) {
        Select<Users_> query = Select
                .from(Users_.class)
                .where(user -> user.ID().eq(id));

        return db.run(query).first(Users.class);
    }

    @Override
    public boolean existByName(String name) {
        return false;
    }
}
