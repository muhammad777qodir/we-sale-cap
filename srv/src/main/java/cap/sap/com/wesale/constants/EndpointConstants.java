package cap.sap.com.wesale.constants;

public interface EndpointConstants {

    String api = "/api";

    String version1 = "/v1";

    String attachment = "/file";

    String attachmentSource = api + version1 + attachment;


    String byId = "/{id}";

}
