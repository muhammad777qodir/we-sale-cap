package cap.sap.com.wesale.enums;

public enum RoleEnum{

    MANAGER{
        @Override
        public String getName() {
            return "Manager";
        }
    },
    EMPLOYEE{
        @Override
        public String getName() {
            return "Employee";
        }
    };

    public abstract String getName();
}
