using { my.bookshop as db } from '../db/schema';

@path : 'catalog-service'
service CatalogService {

entity Book as projection on db.Book excluding{ deleted };

action deleteBook(ID : String) returns String;

entity Author @(requires: 'Employee') as projection on db.Author excluding{ deleted };

action deleteAuthor(ID : String);

entity BookType @(requires: 'Worker') as projection on db.BookType excluding{ deleted };

action deleteBookType(ID : String);

entity Attachment as projection on db.Attachment excluding{ content,deleted };

entity Users as projection on db.Users excluding{ deleted };

entity UserBook as projection on db.UserBook;

entity Comment as projection on db.Comment excluding{ deleted };

action deleteComment(ID : String);

type UserInformation{
name :  String;
id :  String;
roles :  String;
tenant :  String;
}

action getUserInfo() returns UserInformation;

}

@path : 'education-service'
service EducationService{

entity Student as projection on db.Student;

entity School as projection on db.School;

action deleteSchool(ID : String);

}